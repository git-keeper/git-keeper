To release a new version of git-keeper, follow the steps below.

* Update the `VERSION` file with the new version number.
* Run `bump_version.py` to propagate the new version number to all packages
